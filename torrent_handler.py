import xml.sax
from typing import List, Optional


class TorrentItem:
    def __init__(self):
        self.title = ""
        self.pub_date = ""
        self.size = 0
        self.link = ""
        self.seeders = 0
        self.peers = 0


class TorrentHandler(xml.sax.handler.ContentHandler):
    def __init__(self):
        super().__init__()
        self._current_tag = ""
        self._current_data = ""
        self._current_item = TorrentItem()
        self.items = []  # type: List[TorrentItem]
        self._current_attributes = None  # type: Optional[xml.sax.xmlreader.AttributesImpl]

    # Call when an element starts
    def startElement(self, tag: str, attributes: xml.sax.xmlreader.AttributesImpl):
        if tag == "item":
            self.items.append(self._current_item)
            self._current_item = TorrentItem()
        self._current_tag = tag
        self._current_attributes = attributes

        if tag == "torznab:attr":
            name = attributes["name"]
            if name == "seeders":
                self._current_item.seeders = int(attributes["value"])
            elif name == "peers":
                self._current_item.peers = int(attributes["value"])

    # Call when an elements ends
    def endElement(self, tag):
        if self._current_tag == "title":
            self._current_item.title = self._current_data.strip()
        elif self._current_tag == "pubDate":
            self._current_item.pub_date = self._current_data.strip()
        elif self._current_tag == "size":
            self._current_item.size = int(self._current_data.strip())
        elif self._current_tag == "link":
            self._current_item.link = self._current_data.strip()

        self._current_tag = ""
        self._current_data = ""

    # Call when a character is read
    def characters(self, content):
        self._current_data += content

import logging
import sys
import traceback

from config import Config
from files import Files
from home_rho_bot import HomeRhoBot


def exception_handler(err_type: Exception, value, tb: traceback):
    logger.exception(f"Uncaught exception: [{err_type.__name__}]: {str(value)}"
                     f"\n{''.join(traceback.extract_tb(tb).format())}")


if __name__ == '__main__':
    logging.basicConfig(filename=Files.log, level=logging.INFO,
                        format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s",)
    logger = logging.getLogger(__name__)
    sys.excepthook = exception_handler

    bot = HomeRhoBot(Config())
    bot.idle()

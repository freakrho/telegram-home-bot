import xml.sax
from io import StringIO

from torrent_handler import TorrentHandler

if __name__ == "__main__":
    xml_data = """<?xml version="1.0" encoding="UTF-8"?>
                <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:torznab="http://torznab.com/schemas/2015/feed">
                  <channel>
                    <atom:link href="http://jackett:9117/" rel="self" type="application/rss+xml" />
                    <title>AggregateSearch</title>
                    <description>This feed includes all configured trackers</description>
                    <link>http://127.0.0.1/</link>
                    <language>en-US</language>
                    <category>search</category>
                    <item>
                      <title>SCARFACE.mkv</title>
                      <guid>magnet:?xt=urn:btih:db815f36af87287a3597cf74bb2ac3c86651ef20&amp;dn=SCARFACE.mkv&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce</guid>
                      <jackettindexer id="filelisting">FileListing</jackettindexer>
                      <type>public</type>
                      <comments>https://filelisting.com/scarface.mkv-db815f36af87287a3597cf74bb2ac3c86651ef20.html</comments>
                      <pubDate>Mon, 21 Mar 2022 21:58:48 -0300</pubDate>
                      <size>82463375360</size>
                      <description />
                      <link>magnet:?xt=urn:btih:db815f36af87287a3597cf74bb2ac3c86651ef20&amp;dn=SCARFACE.mkv&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce</link>
                      <category>8000</category>
                      <category>100003</category>
                      <enclosure url="magnet:?xt=urn:btih:db815f36af87287a3597cf74bb2ac3c86651ef20&amp;dn=SCARFACE.mkv&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce" length="82463375360" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="8000" />
                      <torznab:attr name="category" value="100003" />
                      <torznab:attr name="seeders" value="21" />
                      <torznab:attr name="peers" value="24" />
                      <torznab:attr name="magneturl" value="magnet:?xt=urn:btih:db815f36af87287a3597cf74bb2ac3c86651ef20&amp;dn=SCARFACE.mkv&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce" />
                      <torznab:attr name="infohash" value="db815f36af87287a3597cf74bb2ac3c86651ef20" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.BluRay.UHD.HEVC.HDR.REMUX.2160p.mkv</title>
                      <guid>https://freshmeat.io/t/4515276</guid>
                      <jackettindexer id="freshmeat">freshMeat</jackettindexer>
                      <type>public</type>
                      <comments>https://freshmeat.io/t/4515276</comments>
                      <pubDate>Tue, 07 Apr 2020 09:56:46 -0300</pubDate>
                      <size>84181360640</size>
                      <description>Russian Federation</description>
                      <link>http://jackett:9117/dl/freshmeat/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUyMnIwWHdRakQySWs1SHY3V0IzSXU1Znh1Q1FIUnVtTzVGZUdjUk9aVUhPUTBlOGRSV2xyZzhpaG1tUnRVenRKOW9ydDV5eG56aEJodWZSUEQ2dHFTMDJlVklPa05mVU05SjZWeUVBV3RyMzJtUmxsZjczclp2NlA1MWRmUkc4VXc&amp;file=Scarface.1983.BluRay.UHD.HEVC.HDR.REMUX.2160p.mkv</link>
                      <category>8000</category>
                      <category>100003</category>
                      <enclosure url="http://jackett:9117/dl/freshmeat/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUyMnIwWHdRakQySWs1SHY3V0IzSXU1Znh1Q1FIUnVtTzVGZUdjUk9aVUhPUTBlOGRSV2xyZzhpaG1tUnRVenRKOW9ydDV5eG56aEJodWZSUEQ2dHFTMDJlVklPa05mVU05SjZWeUVBV3RyMzJtUmxsZjczclp2NlA1MWRmUkc4VXc&amp;file=Scarface.1983.BluRay.UHD.HEVC.HDR.REMUX.2160p.mkv" length="84181360640" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="8000" />
                      <torznab:attr name="category" value="100003" />
                      <torznab:attr name="seeders" value="16" />
                      <torznab:attr name="peers" value="16" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.1080p.BDRip.Goblin.Eng.mkv</title>
                      <guid>https://freshmeat.io/t/4514022</guid>
                      <jackettindexer id="freshmeat">freshMeat</jackettindexer>
                      <type>public</type>
                      <comments>https://freshmeat.io/t/4514022</comments>
                      <pubDate>Tue, 07 Apr 2020 09:57:24 -0300</pubDate>
                      <size>17501990912</size>
                      <description>Russian Federation</description>
                      <link>http://jackett:9117/dl/freshmeat/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUwaVphUXRYcnAydmp2VlhTam51Yzc4c05fM29Wa0FCUHk0QkI3aXdlTDE5S2wxUWtWRXlvQmlaRmsxemNLc050TGtHZFBFTDU1Zll2M2VHcVg5TEI2R1VmOExwVHRqazZXREp5Ml9TbzdCSjJLVmJwSUdCMDdERDBIWjVjQm04T1k&amp;file=Scarface.1983.1080p.BDRip.Goblin.Eng.mkv</link>
                      <category>8000</category>
                      <category>100003</category>
                      <enclosure url="http://jackett:9117/dl/freshmeat/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUwaVphUXRYcnAydmp2VlhTam51Yzc4c05fM29Wa0FCUHk0QkI3aXdlTDE5S2wxUWtWRXlvQmlaRmsxemNLc050TGtHZFBFTDU1Zll2M2VHcVg5TEI2R1VmOExwVHRqazZXREp5Ml9TbzdCSjJLVmJwSUdCMDdERDBIWjVjQm04T1k&amp;file=Scarface.1983.1080p.BDRip.Goblin.Eng.mkv" length="17501990912" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="8000" />
                      <torznab:attr name="category" value="100003" />
                      <torznab:attr name="seeders" value="58" />
                      <torznab:attr name="peers" value="58" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.2160p.BluRay.REMUX.HEVC.DTS-X.7.1-FGT</title>
                      <guid>magnet:?xt=urn:btih:c99b3d945e6079e0114426f19b68a34e1b1a7d59&amp;dn=Scarface.1983.2160p.BluRay.REMUX.HEVC.DTS-X.7.1-FGT&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce</guid>
                      <jackettindexer id="filelisting">FileListing</jackettindexer>
                      <type>public</type>
                      <comments>https://filelisting.com/scarface.1983.2160p.bluray.remux.hevc.dts-x.7.1-fgt-c99b3d945e6079e0114426f19b68a34e1b1a7d59.html</comments>
                      <pubDate>Mon, 21 Mar 2022 21:58:48 -0300</pubDate>
                      <size>87509958656</size>
                      <description />
                      <link>magnet:?xt=urn:btih:c99b3d945e6079e0114426f19b68a34e1b1a7d59&amp;dn=Scarface.1983.2160p.BluRay.REMUX.HEVC.DTS-X.7.1-FGT&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce</link>
                      <category>8000</category>
                      <category>100003</category>
                      <enclosure url="magnet:?xt=urn:btih:c99b3d945e6079e0114426f19b68a34e1b1a7d59&amp;dn=Scarface.1983.2160p.BluRay.REMUX.HEVC.DTS-X.7.1-FGT&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce" length="87509958656" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="8000" />
                      <torznab:attr name="category" value="100003" />
                      <torznab:attr name="seeders" value="10" />
                      <torznab:attr name="peers" value="13" />
                      <torznab:attr name="magneturl" value="magnet:?xt=urn:btih:c99b3d945e6079e0114426f19b68a34e1b1a7d59&amp;dn=Scarface.1983.2160p.BluRay.REMUX.HEVC.DTS-X.7.1-FGT&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fexplodie.org%3A6969%2Fannounce&amp;tr=http%3A%2F%2Ftracker1.itzmx.com%3A8080%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.stealth.si%3A80%2Fannounce" />
                      <torznab:attr name="infohash" value="c99b3d945e6079e0114426f19b68a34e1b1a7d59" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface 1983 4K HDR 2160p BDRip Ita Eng x265-NAHOM</title>
                      <guid>https://torrentparadise.cc/torrent/48073281/scarface-1983-4k-hdr-2160p-bdrip-ita-eng-x265-nahom</guid>
                      <jackettindexer id="torrentparadise">TorrentParadise</jackettindexer>
                      <type>public</type>
                      <comments>https://torrentparadise.cc/torrent/48073281/scarface-1983-4k-hdr-2160p-bdrip-ita-eng-x265-nahom</comments>
                      <pubDate>Tue, 25 May 2021 21:58:49 -0300</pubDate>
                      <size>20830590976</size>
                      <description>Movies</description>
                      <link>http://jackett:9117/dl/torrentparadise/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxRDRFaFQzMnZGbnBZaUF1SkpxNnlZSUxSYVROejNuT2pPT0VBUFdFUXFQZnNQX2F4Qmh4dVpXSjRYM0l0eFBpOG15aDRKR0NNZ3FaS2JSdkl2eEwyNDMyaE9IQTlYc01ZSlRUaUdVbUI5TDNHbzJZSkR5RlNoYi10RGRhN2NsRzVoSldMR253VG1Td3VfU3B2RHRrdm02VElGaWRtRTNlaWo5MVI1RnNJZkFFbEEwUGctWHFhM01PTmlxd3pyQWlIU2hCUGNlMktBN1NOaGdxQ08ydjRE&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265-NAHOM</link>
                      <category>2000</category>
                      <enclosure url="http://jackett:9117/dl/torrentparadise/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxRDRFaFQzMnZGbnBZaUF1SkpxNnlZSUxSYVROejNuT2pPT0VBUFdFUXFQZnNQX2F4Qmh4dVpXSjRYM0l0eFBpOG15aDRKR0NNZ3FaS2JSdkl2eEwyNDMyaE9IQTlYc01ZSlRUaUdVbUI5TDNHbzJZSkR5RlNoYi10RGRhN2NsRzVoSldMR253VG1Td3VfU3B2RHRrdm02VElGaWRtRTNlaWo5MVI1RnNJZkFFbEEwUGctWHFhM01PTmlxd3pyQWlIU2hCUGNlMktBN1NOaGdxQ08ydjRE&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265-NAHOM" length="20830590976" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="2000" />
                      <torznab:attr name="seeders" value="40" />
                      <torznab:attr name="peers" value="45" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface 1983 4K HDR 2160p BDRip Ita Eng x265-NAHOM</title>
                      <guid>https://kickasstorrents.to/scarface-1983-4k-hdr-2160p-bdrip-ita-eng-x265-nahom-t4849385.html</guid>
                      <jackettindexer id="kickasstorrents-to">kickasstorrents.to</jackettindexer>
                      <type>public</type>
                      <comments>https://kickasstorrents.to/scarface-1983-4k-hdr-2160p-bdrip-ita-eng-x265-nahom-t4849385.html</comments>
                      <pubDate>Tue, 25 May 2021 21:58:49 -0300</pubDate>
                      <size>20830590976</size>
                      <description />
                      <link>http://jackett:9117/dl/kickasstorrents-to/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxN0FRMmxULWM0eTZnbnB4aHhNWVVFdXNtNFZtdGdBTWttX2dGWDdKMzktSWNTUlVUd0I2Q0dxODdTdzhGNmNmaVhHTm1VdVRnUUJuTXdkOGxheXRkd2ZXZFI2QVl5ZGlOZERaTzRWRm1nSm5iUzBjejlMVHdsMV9LSmlib0Z4U0U3T29LUEI0d1JBWm1OMGVyZ3l5OW9iaEQ1SkRxVWRBU3V4YXdMQThqZmg3YVp1Rm9RTldCVXFJVXRrLTlYLThtY2pKN1VHUzlrTnlJODVaMUpHYVpF&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265-NAHOM</link>
                      <category>2000</category>
                      <category>112696</category>
                      <enclosure url="http://jackett:9117/dl/kickasstorrents-to/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxN0FRMmxULWM0eTZnbnB4aHhNWVVFdXNtNFZtdGdBTWttX2dGWDdKMzktSWNTUlVUd0I2Q0dxODdTdzhGNmNmaVhHTm1VdVRnUUJuTXdkOGxheXRkd2ZXZFI2QVl5ZGlOZERaTzRWRm1nSm5iUzBjejlMVHdsMV9LSmlib0Z4U0U3T29LUEI0d1JBWm1OMGVyZ3l5OW9iaEQ1SkRxVWRBU3V4YXdMQThqZmg3YVp1Rm9RTldCVXFJVXRrLTlYLThtY2pKN1VHUzlrTnlJODVaMUpHYVpF&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265-NAHOM" length="20830590976" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="2000" />
                      <torznab:attr name="category" value="112696" />
                      <torznab:attr name="seeders" value="29" />
                      <torznab:attr name="peers" value="41" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface 1983 4K HDR 2160p BDRip Ita Eng x265 NAHOM</title>
                      <guid>https://1337x.to/torrent/4849385/Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM/</guid>
                      <jackettindexer id="1337x">1337x</jackettindexer>
                      <type>public</type>
                      <comments>https://1337x.to/torrent/4849385/Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM/</comments>
                      <pubDate>Mon, 26 Apr 2021 00:00:00 -0300</pubDate>
                      <size>20856360960</size>
                      <description />
                      <link>http://jackett:9117/dl/1337x/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxN1lQNGt4N19UaEJmY0JHWENWZmxBdXZ6ZUpEU2VkWXhpSi1LZDhSMU5HQy1CYzFDMkUwUHhjYkcyZkkzY21RMlZ0R3AxOVhqRVNGQlFDRmNOZlhvRERMaXZ2ZFMtbmVhdkZKQUtKTDRUS2R3WXNRVjMwbGF4ODVjUVZNbkhQZXUzam13dzdnTjV6ZkNIYXBhVEJaWXpybmVKU193MjdYaHcyUHNFM3V2VTVySF91MGtZUkMzay1mQW1ab0dmM1hBTU1femM4MXctcW43ZGR5SnA1N3ZU&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265+NAHOM</link>
                      <category>2045</category>
                      <category>100076</category>
                      <enclosure url="http://jackett:9117/dl/1337x/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxN1lQNGt4N19UaEJmY0JHWENWZmxBdXZ6ZUpEU2VkWXhpSi1LZDhSMU5HQy1CYzFDMkUwUHhjYkcyZkkzY21RMlZ0R3AxOVhqRVNGQlFDRmNOZlhvRERMaXZ2ZFMtbmVhdkZKQUtKTDRUS2R3WXNRVjMwbGF4ODVjUVZNbkhQZXUzam13dzdnTjV6ZkNIYXBhVEJaWXpybmVKU193MjdYaHcyUHNFM3V2VTVySF91MGtZUkMzay1mQW1ab0dmM1hBTU1femM4MXctcW43ZGR5SnA1N3ZU&amp;file=Scarface+1983+4K+HDR+2160p+BDRip+Ita+Eng+x265+NAHOM" length="20856360960" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="2045" />
                      <torznab:attr name="category" value="100076" />
                      <torznab:attr name="seeders" value="24" />
                      <torznab:attr name="peers" value="28" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.4K.HDR.2160p.BDRip Ita Eng x265-NAHOM</title>
                      <guid>https://isohunt.nz/torrent_details/t3-4849385/Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM</guid>
                      <jackettindexer id="isohunt2">Isohunt2</jackettindexer>
                      <type>public</type>
                      <comments>https://isohunt.nz/torrent_details/t3-4849385/Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM</comments>
                      <pubDate>Thu, 24 Jun 2021 21:58:49 -0300</pubDate>
                      <size>20830590976</size>
                      <description />
                      <link>http://jackett:9117/dl/isohunt2/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUycUxsVFZiaFhTOWlTV3k0c0I4NmJQOFNVYzV0bC1Db253Wl82bzlmODNPRG1kTFY5S01GVUN5V2tyVVlTam1IVXE5WldINXJCR0pYX2p6Y1JVY25wQk0wMGx2czRkNlNCYlBOWVNlUjF3NGd0dzN5QUJaRHNaTkVjcnNjRDQwRU9Cc2ZNRi1RTEFpNW1QQ1k1bFhjaHNOSTBvN2tmMGN3VU1yMkp3UkNOMDBFbmtwbkZLZW10Zk5sWGUtTUczMUc3czJDbG1zcEdyYVdFRXA3N0xwZWt0OVlCYkxJM1k1enNLMy1QaVZIak9tdw&amp;file=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM</link>
                      <category>2000</category>
                      <category>100005</category>
                      <enclosure url="http://jackett:9117/dl/isohunt2/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUycUxsVFZiaFhTOWlTV3k0c0I4NmJQOFNVYzV0bC1Db253Wl82bzlmODNPRG1kTFY5S01GVUN5V2tyVVlTam1IVXE5WldINXJCR0pYX2p6Y1JVY25wQk0wMGx2czRkNlNCYlBOWVNlUjF3NGd0dzN5QUJaRHNaTkVjcnNjRDQwRU9Cc2ZNRi1RTEFpNW1QQ1k1bFhjaHNOSTBvN2tmMGN3VU1yMkp3UkNOMDBFbmtwbkZLZW10Zk5sWGUtTUczMUc3czJDbG1zcEdyYVdFRXA3N0xwZWt0OVlCYkxJM1k1enNLMy1QaVZIak9tdw&amp;file=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM" length="20830590976" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="2000" />
                      <torznab:attr name="category" value="100005" />
                      <torznab:attr name="seeders" value="24" />
                      <torznab:attr name="peers" value="24" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.4K.HDR.2160p.BDRip Ita Eng x265-NAHOM</title>
                      <guid>magnet:?xt=urn:btih:C339588AE48527878F6ECDBD2B43518687482D1E&amp;dn=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM&amp;tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.uw0.xyz%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.com%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.me%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.open-internet.nl%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.sloppyta.co%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.moeking.me%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.nitrix.me%3A443%2Fannounce&amp;tr=https%3A%2F%2Ftracker.tamersunion.org%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fcoppersurfer.tk%3A6969%2Fannounce</guid>
                      <jackettindexer id="kickasstorrents-ws">kickasstorrents.ws</jackettindexer>
                      <type>public</type>
                      <comments>https://kickass.ws/t3-Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM-tt4849385.html</comments>
                      <pubDate>Thu, 24 Jun 2021 21:58:49 -0300</pubDate>
                      <size>20830590976</size>
                      <description />
                      <link>magnet:?xt=urn:btih:C339588AE48527878F6ECDBD2B43518687482D1E&amp;dn=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM&amp;tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.uw0.xyz%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.com%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.me%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.open-internet.nl%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.sloppyta.co%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.moeking.me%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.nitrix.me%3A443%2Fannounce&amp;tr=https%3A%2F%2Ftracker.tamersunion.org%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fcoppersurfer.tk%3A6969%2Fannounce</link>
                      <category>2000</category>
                      <category>112696</category>
                      <enclosure url="magnet:?xt=urn:btih:C339588AE48527878F6ECDBD2B43518687482D1E&amp;dn=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM&amp;tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.uw0.xyz%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.com%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.me%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.open-internet.nl%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.sloppyta.co%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.moeking.me%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.nitrix.me%3A443%2Fannounce&amp;tr=https%3A%2F%2Ftracker.tamersunion.org%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fcoppersurfer.tk%3A6969%2Fannounce" length="20830590976" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="2000" />
                      <torznab:attr name="category" value="112696" />
                      <torznab:attr name="seeders" value="24" />
                      <torznab:attr name="peers" value="28" />
                      <torznab:attr name="magneturl" value="magnet:?xt=urn:btih:C339588AE48527878F6ECDBD2B43518687482D1E&amp;dn=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM&amp;tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.uw0.xyz%3A6969%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.com%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.to%3A2710%2Fannounce&amp;tr=udp%3A%2F%2F9.rarbg.me%3A2710%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Fexodus.desync.com%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.open-internet.nl%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.sloppyta.co%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.moeking.me%3A6969%2Fannounce&amp;tr=https%3A%2F%2Ftracker.nitrix.me%3A443%2Fannounce&amp;tr=https%3A%2F%2Ftracker.tamersunion.org%3A443%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337%2Fannounce&amp;tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fcoppersurfer.tk%3A6969%2Fannounce" />
                      <torznab:attr name="infohash" value="C339588AE48527878F6ECDBD2B43518687482D1E" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                    <item>
                      <title>Scarface.1983.4K.HDR.2160p.BDRip Ita Eng x265-NAHOM</title>
                      <guid>https://gw1.torlook.info/dl/6/jZLvasIwFMWfRr-IoW36JxVkqK04xjbYniCm11q0SbhNWevTL8UKA50t5EPIvT_OOfem5LkEs3hpzLJGudib4rjYUBoHjK1SnwVexCK2DdNNsk68tU8Dl4Us8pmXuOk0k8tvwfHABRA3ZpT4b2SXfBHPDR1N1slXoWevhs9Smc8aLwzmH6vd5_vU4LLO9ISuJt7WHoNcnACJUYggDYGaKMxt2Q9cW-ZSqloK-A-rfxzStBf7HsZh_ASICXLc50So0r55keuMaDZqfG8Jw70310rbqN0d-7AupdETDhqV1RXJoGql6CMM5P0rNS-kAbSbJvL8mDwao6s7tjorrVvDrWK3EJ-O0CsVnArZj2O0kiwMFs0Vutd5zBheAla1LJS8fZlRDi-ATsbb63IHBn9jzgDiaNXmmiPPigp6xYElCDu_ziMeOr-ne-AX</guid>
                      <jackettindexer id="torlook">Torlook</jackettindexer>
                      <type>public</type>
                      <comments>http://kickass.ws/t3-Scarface-1983-4K-HDR-2160p-BDRip-Ita-Eng-x265-NAHOM-tt4849385.html</comments>
                      <pubDate>Fri, 18 Jun 2021 18:00:00 -0300</pubDate>
                      <size>20830590976</size>
                      <description />
                      <link>http://jackett:9117/dl/torlook/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxZW5qTm1iM2wyM2gxMzVvVHVGQ0FEWllRLWxjWFBkbGl6NUlrMG5sOERzam9NZG9qZEZvWTBONnFmcmtHTGFyNWFKLUZ1X1F1SGJWNEJIbmEyTnhqRkR2T01TS1R3M25HYzcxQmZraUh5cEVFUjZiWUFqSTR6aEh1Y1dZWEc0Zl95YXctMkx3MnBmR1lSb2xhR2ZqUTBfTTk3bzV5ZW5PM2hQYUtxeWlmbVFTd3pVVFRRVXdHVzA2Z0YzLThUSEJfdVg4LTUtRVBjeHgwNFhvR1JidGQ4UTd4N1dBcENoWGpDVEVlenpuZjBJZlV4NXc1aURpMDNoZV90RERXdHZBZHJ6OGx0RlpZdndXLU1uM0RzOVBJak5VVl8zTnV1dlVpQTQ0SVlYOVIzcnQ2cGRyX293anZ0M195a1ZsUGZ3T2tkYWNqcmM5bExDNjRkS1VDcjBoS1VkT1RJRlpKN2lDX3AwZkg4ejZyZHZMWUtWeVVIV3NLVjJaTGZTaW1sc2VBR1g5akw3SmE2di1qZWsyWWppd0pydHR0NEk0NVRXUllNdmRmY0N6REJwNGtuNEUyZFVMajcxcTR3R0RiV0trU2VWWFEyOHdIV3hwazVSeUphT005ZjItMDY2aldIRjlDT0NKT0NIMmdNQ09aTEI3LS1CMWtEajhuNE5YZEFJNmFJcDJtWW1HdEZ2S1BMaERTRGFsZGxEeUtaUDc5eGRfaUc0Nnhfb18xanE1RWR5TllhbE1KNTJnRE05YmU3akJva25JVW1pN0lhR184NlJHWXdYNHU2bDhjak9yM2tiRUp6OUdjWFV5cEdQTTVZSHpjTVIwQkVhNVZVYWVMdmNUQ1hzMXRBQ3JGaU9HcXBUcGpyMDZyNExHRTBMaTF5N1ZJeW1MSUYxNUQ0Q2lwRDlPS3VVVGVJRUFhNExqV25wSTZ2aVJyN0JXR21mSGVlMVdveHpBdXU&amp;file=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM</link>
                      <category>8000</category>
                      <category>100001</category>
                      <enclosure url="http://jackett:9117/dl/torlook/?jackett_apikey=g32zjmclz7bhdjohfd28p278bfv5fjvw&amp;path=Q2ZESjhESm1RQlRlckZWS2pfOFJNQXc4NWUxZW5qTm1iM2wyM2gxMzVvVHVGQ0FEWllRLWxjWFBkbGl6NUlrMG5sOERzam9NZG9qZEZvWTBONnFmcmtHTGFyNWFKLUZ1X1F1SGJWNEJIbmEyTnhqRkR2T01TS1R3M25HYzcxQmZraUh5cEVFUjZiWUFqSTR6aEh1Y1dZWEc0Zl95YXctMkx3MnBmR1lSb2xhR2ZqUTBfTTk3bzV5ZW5PM2hQYUtxeWlmbVFTd3pVVFRRVXdHVzA2Z0YzLThUSEJfdVg4LTUtRVBjeHgwNFhvR1JidGQ4UTd4N1dBcENoWGpDVEVlenpuZjBJZlV4NXc1aURpMDNoZV90RERXdHZBZHJ6OGx0RlpZdndXLU1uM0RzOVBJak5VVl8zTnV1dlVpQTQ0SVlYOVIzcnQ2cGRyX293anZ0M195a1ZsUGZ3T2tkYWNqcmM5bExDNjRkS1VDcjBoS1VkT1RJRlpKN2lDX3AwZkg4ejZyZHZMWUtWeVVIV3NLVjJaTGZTaW1sc2VBR1g5akw3SmE2di1qZWsyWWppd0pydHR0NEk0NVRXUllNdmRmY0N6REJwNGtuNEUyZFVMajcxcTR3R0RiV0trU2VWWFEyOHdIV3hwazVSeUphT005ZjItMDY2aldIRjlDT0NKT0NIMmdNQ09aTEI3LS1CMWtEajhuNE5YZEFJNmFJcDJtWW1HdEZ2S1BMaERTRGFsZGxEeUtaUDc5eGRfaUc0Nnhfb18xanE1RWR5TllhbE1KNTJnRE05YmU3akJva25JVW1pN0lhR184NlJHWXdYNHU2bDhjak9yM2tiRUp6OUdjWFV5cEdQTTVZSHpjTVIwQkVhNVZVYWVMdmNUQ1hzMXRBQ3JGaU9HcXBUcGpyMDZyNExHRTBMaTF5N1ZJeW1MSUYxNUQ0Q2lwRDlPS3VVVGVJRUFhNExqV25wSTZ2aVJyN0JXR21mSGVlMVdveHpBdXU&amp;file=Scarface.1983.4K.HDR.2160p.BDRip+Ita+Eng+x265-NAHOM" length="20830590976" type="application/x-bittorrent" />
                      <torznab:attr name="category" value="8000" />
                      <torznab:attr name="category" value="100001" />
                      <torznab:attr name="seeders" value="24" />
                      <torznab:attr name="peers" value="28" />
                      <torznab:attr name="downloadvolumefactor" value="0" />
                      <torznab:attr name="uploadvolumefactor" value="1" />
                    </item>
                  </channel>
                </rss>"""
    parser = xml.sax.make_parser()
    # turn off namespaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # override the default ContextHandler
    handler = TorrentHandler()
    parser.setContentHandler(handler)

    parser.parse(StringIO(xml_data))
    handler.items.sort(key=lambda x: x.seeders, reverse=True)
    for item in handler.items:
        print(f"[{item.title}]; [{item.pub_date}]; [{item.size}]; seeds: {item.seeders}; leeches: {item.peers}; {item.link}")

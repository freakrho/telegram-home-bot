import asyncio
import os
import zipfile
import enum
import json
import math
import random
import string
import logging
import threading
import time
import xml.sax
from dataclasses import dataclass
from io import StringIO
from pathlib import Path
from typing import Optional, List
from urllib import request
from http.client import HTTPResponse

import requests
from requests import Response
from urlextract import URLExtract
from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton, Chat, ParseMode
from telegram.ext import Updater, CommandHandler, Dispatcher, MessageHandler, Filters, \
    ConversationHandler, CallbackQueryHandler, CallbackContext

from transmission_rpc import Client as TransmissionClient
from qbittorrentapi import Client as QBitClient, TorrentInfoList, TorrentDictionary, APIConnectionError
from transmission_rpc.error import TransmissionConnectError

import database
from config import Config
from database import Database, Table
from files import Files
from torrent_handler import TorrentHandler, TorrentItem


LOOP_WAIT = 0.1
DOWNLOAD_WAIT_PERIOD = 5
ZIP = "application/zip"
ATTEMPTS = 3


class AddMediaState(enum.Enum):
    SEARCH_TERM = 0
    CHOOSE_TORRENT = 1
    CHOOSE_MEDIA_DIR = 2
    CONFIRM = 3


class UploadMediaState(enum.Enum):
    UPLOAD = 0
    CHOSE_NAME = 1
    CHOOSE_MEDIA_DIR = 2


class EnableUserState(enum.Enum):
    WAITING_RESPONSE = 0


@dataclass
class Download:
    id: int
    file: Response
    dir: str
    name: str


class HomeRhoBot:
    def __init__(self, config: Config):
        self._logger = logging.getLogger(__name__)

        self._config = config
        self._load_config()

        self._updater = Updater(config.telegram_token)

        dp = self._updater.dispatcher  # type: Dispatcher

        # Conversations
        done_commands = [
            CommandHandler("done", self._done),
            CommandHandler("cancel", self._done),
        ]
        # Add media
        dp.add_handler(ConversationHandler(
            entry_points=[CommandHandler("addmedia", self._add_media)],
            states={
                AddMediaState.SEARCH_TERM: [
                    MessageHandler(filters=Filters.text & (~Filters.command), callback=self._search_media)
                ],
                AddMediaState.CHOOSE_TORRENT: [CallbackQueryHandler(self._choose_torrent)],
                AddMediaState.CHOOSE_MEDIA_DIR: [CallbackQueryHandler(self._choose_media_dir)],
                AddMediaState.CONFIRM: [CallbackQueryHandler(self._confirm_media_data)],
            },
            fallbacks=done_commands
        ))
        # Upload media
        dp.add_handler(ConversationHandler(
            entry_points=[CommandHandler("uploadmedia", self._upload_media)],
            states={
                UploadMediaState.UPLOAD: [
                    MessageHandler(
                        filters=Filters.text & (~Filters.command),
                        callback=self._received_media_link
                    )
                ],
                UploadMediaState.CHOSE_NAME: [
                    MessageHandler(
                        filters=Filters.text & (~Filters.command),
                        callback=self._name_media
                    )
                ],
                UploadMediaState.CHOOSE_MEDIA_DIR: [CallbackQueryHandler(self._choose_download_dir)],
            },
            fallbacks=done_commands
        ))
        # Enable user
        dp.add_handler(ConversationHandler(
            entry_points=[CommandHandler("enable", self._enable_user_command)],
            states={
                EnableUserState.WAITING_RESPONSE: [
                    MessageHandler(
                        filters=Filters.text & (~Filters.command),
                        callback=self._respond_enable_word
                    ),
                ],
            },
            fallbacks=done_commands
        ))
        # Enable superuser
        dp.add_handler(ConversationHandler(
            entry_points=[CommandHandler("superuser", self._enable_user_command)],
            states={
                EnableUserState.WAITING_RESPONSE: [
                    MessageHandler(
                        filters=Filters.text & (~Filters.command),
                        callback=self._respond_superuser_word
                    ),
                ],
            },
            fallbacks=done_commands
        ))

        # Other commands
        dp.add_handler(CommandHandler("status", self._torrents_status))
        dp.add_handler(CommandHandler("ping", self._ping))
        dp.add_handler(CommandHandler("currentsearches", self._current_searches))
        dp.add_handler(CommandHandler("showconfig", self._show_config))
        dp.add_handler(CommandHandler("setconfig", self._set_config))
        dp.add_handler(CommandHandler("reloadconfig", self._reload_config))

        self._enabled_process = {}
        self._super_user_process = {}

        self._database = None  # type: Optional[Database]
        self._enabled_users_table = None  # type: Optional[Table]
        self._super_users_table = None  # type: Optional[Table]

        self._event_loop_set = False

        self._url_extractor = URLExtract()
        self._download_list = []  # type: List[Download]
        self._complete_downloads = []  # type: List[Download]
        self._download_id = 0
        self._download_thread = threading.Thread(target=self._download_loop)
        self._download_thread.start()

        self._torrent_searches = []
        self._torrent_results = []
        self._current_search = None
        self._torrent_search_thread = threading.Thread(target=self._torrent_search_loop)
        self._torrent_search_thread.start()

    def _load_config(self):
        self._transmission = False
        if self._config.transmission.active:
            try:
                self._transmission_client = TransmissionClient(
                    host="transmission",
                    port=self._config.transmission.port,
                    username=self._config.transmission.user,
                    password=self._config.transmission.password
                )
                self._transmission = True
            except TransmissionConnectError:
                self._config.transmission.active = False

        if self._config.qbittorrent.active:
            try:
                self._qbittorrent_client = QBitClient(
                    host="qbittorrent",
                    port=self._config.qbittorrent.port,
                    username=self._config.qbittorrent.user,
                    password=self._config.qbittorrent.password
                )
                self._qbittorrent_client.auth_log_in()
            except APIConnectionError:
                self._config.qbittorrent.active = False
            
        self._jackett_baseurl = f"{self._config.jackett_url}/api/v2.0/indexers/"
        self._jackett_search_url = self._jackett_baseurl + "all/results/torznab/api?apikey=" \
            + self._config.jackett_token + "&limit={limit}&t=search&q={query}"

    def _download_loop(self):
        while True:
            if len(self._download_list) > 0:
                download = self._download_list.pop()
                req = requests.get(download.file.url)
                if req.status_code == 200:
                    ext = ""
                    content_type = download.file.headers['Content-Type']
                    if content_type == ZIP:
                        ext = ".zip"
                    download_path = f"{download.dir}/{download.name}{ext}"
                    path = Path(download_path)
                    path.write_bytes(req.content)
                    with zipfile.ZipFile(path) as archive:
                        archive.extractall(f"{download.dir}/{download.name}")
                    os.remove(path)
                    self._complete_downloads.append(download)

            time.sleep(LOOP_WAIT)

    def _torrent_search_loop(self):
        while True:
            if len(self._torrent_searches) > 0:
                self._current_search = self._torrent_searches.pop()
                torrents = self.search_torrent("%20".join(self._current_search['search_term'].split(' ')))
                self._torrent_results.append({
                    'search': self._current_search,
                    'result': torrents,
                })
                self._current_search = None
            time.sleep(LOOP_WAIT)

    def _debug(self, message: object):
        self._logger.debug(message)

    def _log(self, message: object):
        self._logger.info(message)

    def _set_event_loop(self):
        if not self._event_loop_set:
            asyncio.set_event_loop(asyncio.new_event_loop())
            self._event_loop_set = True

    def _load_database(self):
        if self._database is not None:
            return
        self._database = Database()
        # Enabled users
        self._enabled_users_table = Table("enabled_users")
        self._enabled_users_table.add_row("user_id", database.STRUCTURE_INT)
        self._database.execute(self._enabled_users_table.query_create())
        # Super users
        self._super_users_table = Table("super_users")
        self._super_users_table.add_row("user_id", database.STRUCTURE_INT)
        self._database.execute(self._super_users_table.query_create())
        self._database.commit()

    def _enable_user(self, user_id, table: Table):
        self._load_database()
        query = table.query_insert({"user_id": user_id})
        self._database.execute(query)
        self._database.commit()

    def is_user(self, table: Table, user_id: str):
        result = self._database.select(table, f"user_id={user_id}")
        return len(result) > 0

    def user_enabled(self, user_id):
        self._load_database()
        return self.is_user(self._enabled_users_table, user_id)

    def is_super_user(self, user_id):
        self._load_database()
        return self.is_user(self._super_users_table, user_id)

    def search_torrent(self, query: str) -> List[TorrentItem]:
        url = self._jackett_search_url.format(limit=self._config.torrent_search_limit, query=query)
        self._log(f"Query url: {url}")
        response = request.urlopen(url, timeout=2 * 60)  # type: HTTPResponse
        if response.status != 200:
            self._logger.error(f"Error {response.status} fetching query {response.msg}")
            return []

        data = response.read().decode("utf-8")
        parser = xml.sax.make_parser()
        # turn off namespaces
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)

        # override the default ContextHandler
        handler = TorrentHandler()
        parser.setContentHandler(handler)

        parser.parse(StringIO(data))
        handler.items.sort(key=lambda x: x.seeders, reverse=True)
        return handler.items

    @staticmethod
    def _get_poll_items(poll_data, config: Config):
        item_list = []
        items = poll_data["items"]
        for i in range(min(config.torrent_display_limit, len(items))):
            item = items[i]
            size = item.size / (1024 ** 3)
            truncated = False
            tail = f"; {item.seeders}⏫ {item.peers}⏬; {'{0:.2f}'.format(size)} GB"
            if len(item.title) + len(tail) > config.max_item_title_length:
                truncated = True
                item.title = item.title[0:config.max_item_title_length - len(tail) - 3]
            item_list.append(
                f"{item.title}{'...' if truncated else ''}{tail}")
        item_list.append("None")
        return item_list

    @staticmethod
    def randomword(length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for _ in range(length))

    def _add_torrent(self, link: str, download_dir: str, category: str = None) -> str:
        torrent_name = ""

        if self._config.qbittorrent.active:
            self._qbittorrent_client.torrents.add(urls=[link], save_path=download_dir, category=category)

        if self._transmission:
            torrent = self._transmission_client.add_torrent(link, download_dir=download_dir)
            torrent_name = torrent.name

        return torrent_name

    def idle(self):
        self._updater.start_polling()
        self._updater.idle()

    @staticmethod
    def _is_compressed(content_type: str):
        if content_type == ZIP:
            return True
        return False

    # UPLOAD MEDIA
    def _upload_media(self, update: Update, _):
        if not self.user_enabled(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return ConversationHandler.END
        update.message.chat.send_message(
            "Upload an archive to https://tmp.ninja/ and send me the link", disable_web_page_preview=True)
        return UploadMediaState.UPLOAD

    def _received_media_link(self, update: Update, context: CallbackContext):
        urls = self._url_extractor.find_urls(update.message.text)
        files = []
        for url in urls:
            response = requests.request('HEAD', url)
            if response.status_code == 200:
                if self._is_compressed(response.headers['Content-Type']):
                    files.append(response)

        if len(files) > 0:
            file = files[0]
            context.user_data['file'] = file
            update.message.chat.send_message("What should I name this?")

            return UploadMediaState.CHOSE_NAME
        return self._done(update, context)

    def _name_media(self, update: Update, context: CallbackContext):
        context.user_data['filename'] = update.message.text
        self._send_media_buttons(update.message.chat)

        return UploadMediaState.CHOOSE_MEDIA_DIR

    def _choose_download_dir(self, update: Update, context: CallbackContext):
        # start downloads
        directory = update.callback_query.data
        file = context.user_data['file']  # type: Response
        download = Download(id=self._download_id, file=file, dir=directory, name=context.user_data['filename'])
        self._download_id += 1
        self._download_list.append(download)
        context.user_data['download'] = download
        context.user_data['chat'] = update.callback_query.message.chat

        update.callback_query.message.delete()

        context.job_queue.run_repeating(
            self._download_wait,
            DOWNLOAD_WAIT_PERIOD,
            context=context.user_data
        )

        update.callback_query.message.chat.send_message("I'll let you know when the download is done")
        return self._done(update, context)

    def _download_wait(self, context: CallbackContext):
        data = context.job.context  # type: dict
        download = data['download']
        for i in range(len(self._complete_downloads)):
            if self._complete_downloads[i].id == download.id:
                self._complete_downloads.pop(i)
                data['chat'].send_message(f"Finished downloading **{download.name}**", parse_mode=ParseMode.MARKDOWN_V2)
                context.job.schedule_removal()
                return

    # ADD MEDIA
    def _add_media(self, update: Update, _):
        if not self.user_enabled(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return

        update.message.chat.send_message("Enter search term")
        return AddMediaState.SEARCH_TERM

    def _search_media(self, update: Update, context: CallbackContext):
        context.user_data['search_term'] = update.message.text
        msg = update.message.reply_text("Wait while I search...")
        context.user_data['search_message'] = msg
        context.user_data['chat'] = update.message.chat

        self._torrent_searches.append(context.user_data)

        context.job_queue.run_repeating(
            self._media_search_wait,
            DOWNLOAD_WAIT_PERIOD,
            context=context.user_data
        )

        return AddMediaState.CHOOSE_TORRENT

    def _media_search_wait(self, context: CallbackContext):
        data = context.job.context  # type: dict

        for i in range(len(self._torrent_results)):
            search = self._torrent_results[i]
            if search['search']['search_term'] == data['search_term']:
                self._torrent_results.pop(i)
                data['search_message'].delete()
                torrents = search['result']
                data['torrents'] = torrents
                data['page'] = 0
                if len(torrents) == 0:
                    data['chat'].send_message(f"Couldn't find anything for \"{data['search_term']}\"")
                    return
                self._send_torrents(data, data['chat'])
                context.job.schedule_removal()

    def _send_torrents(self, convo: dict, chat: Chat):
        options = []
        torrents = convo['torrents']  # type: List[TorrentItem]
        first = convo['page'] * self._config.torrent_display_limit
        last = min(first + self._config.torrent_display_limit, len(torrents))
        for i in range(first, last):
            item = torrents[i]
            size = item.size / (1024 ** 3)
            options.append([
                InlineKeyboardButton(
                    f"{item.seeders}⏫ {item.peers}⏬; {'{0:.2f}'.format(size)} GB | {item.title}",
                    callback_data=i
                )
            ])

        if len(torrents) > self._config.torrent_display_limit:
            pagination = []
            if first > 0:
                pagination.append(InlineKeyboardButton("⬅", callback_data="prev"))
            if last < len(torrents):
                pagination.append(InlineKeyboardButton("➡", callback_data="next"))
            options.append(pagination)
        chat.send_message("Choose torrent to download", reply_markup=InlineKeyboardMarkup(options))

    def _choose_torrent(self, update: Update, context: CallbackContext):
        pagination = False
        if update.callback_query.data == "prev":
            context.user_data['page'] -= 1
            pagination = True
        if update.callback_query.data == "next":
            context.user_data['page'] += 1
            pagination = True
        if pagination:
            update.callback_query.message.delete()
            self._send_torrents(context.user_data, update.callback_query.message.chat)
            return AddMediaState.CHOOSE_TORRENT

        try:
            index = int(update.callback_query.data)
            context.user_data['selected'] = context.user_data['torrents'][index]
            self._send_media_buttons(update.callback_query.message.chat)
            update.callback_query.message.delete()
            return AddMediaState.CHOOSE_MEDIA_DIR
        except ValueError:
            return self._done(update, context)

    def _choose_media_dir(self, update: Update, context: CallbackContext):
        update.callback_query.message.delete()
        torrent = context.user_data['selected']  # type: TorrentItem
        media_dir = update.callback_query.data
        context.user_data['media_dir'] = media_dir
        size = torrent.size / (1024 ** 3)
        media_desc = f"Torrent:\n    {torrent.title}\n    {torrent.seeders}⏫ {torrent.peers}⏬;" \
                     f" {'{0:.2f}'.format(size)} GB;" \
                     f"\n\nMedia folder:\n    {media_dir}"
        update.callback_query.message.chat.send_message(
            f"{media_desc}\n\nIs this OK?",
            reply_markup=InlineKeyboardMarkup([
                [InlineKeyboardButton("Yes", callback_data="yes")],
                [InlineKeyboardButton("No", callback_data="no")]
            ]),
        )
        return AddMediaState.CONFIRM

    def _confirm_media_data(self, update: Update, context: CallbackContext):
        update.callback_query.message.delete()
        if update.callback_query.data == "yes":
            torrent = context.user_data['selected']  # type: TorrentItem
            media_dir = context.user_data['media_dir']
            self._add_torrent(torrent.link, f"/shared/{media_dir}", media_dir)
            update.callback_query.message.chat.send_message(
                f"Torrent {torrent.title} added to {media_dir}")
            return self._done(update, context)
        else:
            self._send_torrents(context.user_data, update.callback_query.message.chat)
            return AddMediaState.CHOOSE_TORRENT

    @staticmethod
    def _done(update: Update, _):
        chat = None
        if update.message is not None:
            chat = update.message.chat
        elif update.callback_query is not None:
            chat = update.callback_query.message.chat

        if chat is not None:
            chat.send_message("**We're done here**", parse_mode=ParseMode.MARKDOWN_V2)
        return ConversationHandler.END

    def _send_media_buttons(self, chat: Chat):
        options = []
        for media in self._config.media_list:
            options.append([InlineKeyboardButton(media, callback_data=media)])
        chat.send_message("Choose media folder to download to", reply_markup=InlineKeyboardMarkup(options))

    # COMMANDS
    def _generate_progress_bar(self, progress: float) -> str:
        bar = ''
        progress = progress * self._config.progress_bar.length
        length = int(math.floor(progress))
        for i in range(length):
            bar += self._config.progress_bar.full
        if self._config.progress_bar.use_half:
            rem = progress - length
            if 0.2 < rem < 0.8:
                length += 1
                bar += self._config.progress_bar.half
        for i in range(self._config.progress_bar.length - length):
            bar += self._config.progress_bar.empty
        return bar

    def _torrents_status(self, update: Update, context: CallbackContext):
        if not self.user_enabled(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return

        dont_filter = "-a" in context.args

        if self._config.qbittorrent.active:
            info_list = self._qbittorrent_client.torrents_info()  # type: TorrentInfoList
            text = []
            downloading = 0
            for torrent in info_list:  # type: TorrentDictionary
                if not torrent.state_enum.is_downloading:
                    if not dont_filter:
                        continue
                    state = "✅"
                else:
                    state = f"🔄"
                    downloading += 1

                progress = float(torrent['completed']) / torrent['total_size']
                line = f"\\[{torrent['category']}] {state} **{torrent.name}**" \
                       f"\n{torrent['num_seeds']}⏫ {torrent['num_leechs']}⏬"
                if torrent.state_enum.is_downloading:
                    line += f" \\[{self._generate_progress_bar(progress)}] {int(100 * progress)}%"

                text.append(line)

            update.message.reply_markdown(f"Currently downloading {downloading}"
                                          f" torrent{'s' if downloading == 0 or downloading > 1 else ''}\n"
                                          + "\n---------\n".join(text))

    def _enable_user_command(self, update: Update, context: CallbackContext):
        word = self.randomword(10)
        context.user_data['word'] = word
        context.user_data['attempts'] = 0
        with open(Files.word, 'w') as word_file:
            word_file.write(word + "\n")
        update.message.reply_text("Reply with the secret word")
        return EnableUserState.WAITING_RESPONSE

    def _enable_user_check(self, update: Update, context: CallbackContext, table: Table):
        if update.message.text.strip() == context.user_data['word']:
            self._enable_user(update.message.from_user.id, table)
            update.message.reply_text("You are now a verified user")
            return ConversationHandler.END
        else:
            attempts = context.user_data['attempts']
            attempts += 1
            update.message.reply_text(f"Incorrect word {attempts}/{ATTEMPTS}")
            context.user_data['attempts'] = attempts
            if context.user_data['attempts'] >= ATTEMPTS:
                update.message.reply_text(f"No more attempts, try again")
                return ConversationHandler.END
            return EnableUserState.WAITING_RESPONSE

    def _respond_enable_word(self, update: Update, context: CallbackContext):
        self._load_database()
        return self._enable_user_check(update, context, self._enabled_users_table)

    def _respond_superuser_word(self, update: Update, context: CallbackContext):
        self._load_database()
        return self._enable_user_check(update, context, self._super_users_table)

    def _current_searches(self, update: Update, _):
        if not self.is_super_user(update.message.from_user.id):
            return
        text = ""
        count = len(self._torrent_searches)
        if self._current_search is not None:
            text += f"\t💠{self._current_search['search_term']} \n"
            count += 1
        for search in self._torrent_searches:
            text += f"\t💠{search['search_term']} \n"

        text = f"Currently doing {count} searches\n" + text
        update.message.reply_markdown(text)

    def _reload_config(self, update: Update, _):
        if not self.is_super_user(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return
        self._config.load()
        self._load_config()
        update.message.reply_markdown("Config reloaded")

    def _show_config(self, update: Update, _):
        if not self.is_super_user(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return
        config = json.dumps(self._config.data, indent=4)
        self._debug(config)
        update.message.reply_markdown(f"```{config}```")

    def _set_config(self, update: Update, context: CallbackContext):
        if not self.is_super_user(update.message.from_user.id):
            update.message.reply_text("You are not authorized to use this command")
            return

        if len(context.args) < 2:
            return
        path = context.args[0].split('.')
        value = context.args[1]
        data = self._config.data
        for key in path[:-1]:
            if key not in data:
                return
            data = data[key]
        data[path[-1]] = value

        self._config.save()

        update.message.reply_markdown(f"`{context.args[0]}` set to `{value}")

    @staticmethod
    def _ping(update: Update, _):
        update.message.reply_text("Pong")

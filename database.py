import sqlite3
from dataclasses import dataclass
from enum import Enum
from typing import List, Dict, Optional, Any
from files import Files

DATABASE_FILE = "/files/database.db"
ID = "ID"


class DataType(Enum):
    INT = 0
    CHAR = 1
    DATETIME = 2
    FLOAT = 3


@dataclass
class DataStructure:
    type: DataType = DataType.INT
    length: int = 50
    nullable: bool = True
    extra: str = ""

    def type_string(self) -> str:
        result = ""

        if self.type == DataType.INT:
            result += "INT"
        elif self.type == DataType.CHAR:
            result += f"CHAR({self.length})"
        elif self.type == DataType.DATETIME:
            result += "DATETIME"
        elif self.type == DataType.FLOAT:
            result += "FLOAT"

        if self.extra:
            result += " " + self.extra
        if not self.nullable:
            result += " NOT NULL"
        return result

    def format(self, value) -> str:
        if self.type == DataType.INT or self.type == DataType.FLOAT:
            return str(value)
        elif self.type == DataType.CHAR:
            return f"'{value}'"
        elif self.type == DataType.DATETIME:
            return value.strftime("'%Y-%m-%d %H:%M:%S'")


STRUCTURE_INT = DataStructure(
    type=DataType.INT,
    nullable=False
)
STRUCTURE_CHAR = DataStructure(
    type=DataType.CHAR,
    length=50,
    nullable=False
)
STRUCTURE_DATETIME = DataStructure(
    type=DataType.DATETIME,
    nullable=False,
    extra="DEFAULT current_timestamp"
)
STRUCTURE_FLOAT = DataStructure(
    type=DataType.FLOAT,
    nullable=False
)


class Row:
    def __init__(self, name: str, data_structure: DataStructure):
        self.name = name
        self.structure = data_structure

    def query_create(self) -> str:
        return "{name} {type}".format(
            name=self.name,
            type=self.structure.type_string()
        )

    def format_value(self, value) -> str:
        return self.structure.format(value)


class Table:
    def __init__(self, name):
        self.name = name
        self.rows = []  # type: List[Row]

    def get_row(self, name) -> Row:
        for row in self.rows:
            if row.name == name:
                return row

    def query_create(self):
        query = f"CREATE TABLE IF NOT EXISTS {self.name}({ID} INTEGER PRIMARY KEY AUTOINCREMENT,"
        query += ",".join(row.query_create() for row in self.rows)
        query += ");"
        return query

    def add_row(self, name: str, structure: DataStructure):
        self.rows.append(Row(name, structure))

    def query_insert(self, values: Dict[str, Any]):
        keys = values.keys()
        value_list = []
        for key in keys:
            value_list.append(self.get_row(key).format_value(values[key]))
        return "INSERT INTO {name} ({keys}) VALUES ({values});".format(
            name=self.name,
            keys=",".join(keys),
            values=",".join(value_list)
        )

    def query_select(self, keys: Optional[List[str]], where: Optional[str] = None):
        if keys is None:
            keys = list(row.name for row in self.rows)
            keys.insert(0, ID)
        if where is None:
            where = ""
        else:
            where = " WHERE " + where
        return f"SELECT {','.join(keys)} FROM " + self.name + where

    def query_delete(self, where: str):
        return f"DELETE FROM {self.name} WHERE {where};"

    def query_delete_row(self, row_id: int):
        return self.query_delete(f"{ID} = {row_id}")

    def query_update(self, values: Dict[str, Any], where: str):
        return f"UPDATE {self.name} SET " \
               f"{', '.join(f'{key}={self.get_row(key).format_value(values[key])}' for key in values)} WHERE {where}"


class Database:
    def __init__(self):
        self._conn = sqlite3.connect(Files.database)

    def execute(self, query: str) -> sqlite3.Cursor:
        print(f">> {query}")
        return self._conn.execute(query)

    def select(self, table: Table, where: Optional[str] = None) -> List[Dict[str, Any]]:
        keys = list(row.name for row in table.rows)
        keys.insert(0, ID)

        query = table.query_select(keys, where)
        cursor = self._conn.execute(query)
        result = []
        for row in cursor:
            row_dict = {}
            for i in range(len(row)):
                row_dict[keys[i]] = row[i]
            result.append(row_dict)
        return result

    def commit(self):
        self._conn.commit()



FROM python:3
RUN mkdir /usr/src/homerhobot
WORKDIR /usr/src/homerhobot
COPY requirements.txt .
COPY main.py .
COPY home_rho_bot.py .
COPY database.py .
COPY torrent_handler.py .
COPY config.py .
COPY files.py .
RUN pip install -r requirements.txt

CMD [ "python", "main.py" ]

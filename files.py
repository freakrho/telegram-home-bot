import os.path


DATABASE = "database.db"
CONFIG = "config.json"
WORD = "word.txt"
LOG = "log.log"


def get_path(filename):
    if os.path.isdir("/files"):
        return f"/files/{filename}"
    return filename


class Files:
    database = get_path(DATABASE)
    config = get_path(CONFIG)
    word = get_path(WORD)
    log = get_path(LOG)


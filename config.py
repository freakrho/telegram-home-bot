import json
import os
from dataclasses import dataclass
from typing import List

from files import Files


class ConfigInfo:
    def __init__(self, data: dict):
        self.data = data

    def get_value(self, key: str, default):
        if key not in self.data:
            return default
        return self.data[key]

    def get_string(self, key: str, default: str = ''):
        return self.get_value(key, default)

    def get_int(self, key: str, default: int = 0):
        return int(self.get_value(key, default))

    def get_bool(self, key: str, default: bool = False):
        return bool(self.get_value(key, default))


class LoginInfo(ConfigInfo):
    @property
    def user(self) -> str:
        return self.get_string('user')

    @user.setter
    def user(self, value: str):
        self.data['user'] = value

    @property
    def password(self) -> str:
        return self.get_string('password')

    @password.setter
    def password(self, value: str):
        self.data['password'] = value

    @property
    def port(self) -> int:
        return self.get_int('port')

    @port.setter
    def port(self, value: int):
        self.data['port'] = value

    @property
    def active(self) -> bool:
        return self.get_bool('active')

    @active.setter
    def active(self, value: bool):
        self.data['active'] = value


@dataclass
class ProgressBar(ConfigInfo):
    def __init__(self, data: dict):
        super().__init__(data)
        self.data = data

    @property
    def full(self):
        return self.get_string('full', '⬜')

    @full.setter
    def full(self, value: str):
        self.data['full'] = value

    @property
    def empty(self):
        return self.get_string('empty', '⬛')

    @empty.setter
    def empty(self, value: str):
        self.data['empty'] = value

    @property
    def half(self):
        return self.get_string('half', '🔳')

    @half.setter
    def half(self, value: str):
        self.data['half'] = value

    @property
    def length(self):
        return self.get_int('length', 10)

    @length.setter
    def length(self, value: int):
        self.data['length'] = value

    @property
    def use_half(self):
        return self.get_bool('use_half', True)

    @use_half.setter
    def use_half(self, value: bool):
        self.data['use_half'] = value


class Config:
    def __init__(self):
        self.data = {}
        self.telegram_token = ""
        self.jackett_token = ""
        self.torrent_search_limit = 50
        self.torrent_display_limit = 9
        self.max_item_title_length = 100
        self._qbittorrent = None
        self._transmission = None
        self.transmission = LoginInfo({})
        self.qbittorrent = LoginInfo({})
        self.progress_bar = ProgressBar({})
        self.jackett_url = "http://jackett:9117"
        self.media_list = ["Series", "Movies", "Anime", "Music"]

        self.load()

    @property
    def telegram_token(self):
        return self.data['telegram_token']

    @telegram_token.setter
    def telegram_token(self, token):
        self.data['telegram_token'] = token

    @property
    def jackett_token(self):
        return self.data['jackett_token']

    @jackett_token.setter
    def jackett_token(self, token):
        self.data['jackett_token'] = token

    @property
    def jackett_url(self):
        return self.data['jackett_url']

    @jackett_url.setter
    def jackett_url(self, url):
        self.data['jackett_url'] = url

    @property
    def torrent_search_limit(self) -> int:
        return int(self.data['torrent_search_limit'])

    @torrent_search_limit.setter
    def torrent_search_limit(self, value: int):
        self.data['torrent_search_limit'] = value

    @property
    def torrent_display_limit(self) -> int:
        return min(int(self.data['torrent_display_limit']), 9)

    @torrent_display_limit.setter
    def torrent_display_limit(self, value: int):
        self.data['torrent_display_limit'] = value

    @property
    def max_item_title_length(self):
        return min(int(self.data['max_item_title_length']), 100)

    @max_item_title_length.setter
    def max_item_title_length(self, value: int):
        self.data['max_item_title_length'] = value
    
    @property
    def qbittorrent(self) -> LoginInfo:
        if self._qbittorrent is None:
            self._qbittorrent = LoginInfo(self.data['qbittorrent'])
        return self._qbittorrent
    
    @qbittorrent.setter
    def qbittorrent(self, value: LoginInfo):
        self._qbittorrent = value
        self.data['qbittorrent'] = value.data

    @property
    def transmission(self) -> LoginInfo:
        if self._transmission is None:
            self._transmission = LoginInfo(self.data['transmission'])
        return self._transmission
    
    @transmission.setter
    def transmission(self, value: LoginInfo):
        self._transmission = value
        self.data['transmission'] = value.data

    @property
    def media_list(self) -> List[str]:
        return self.data['media_list']

    @media_list.setter
    def media_list(self, value: List[str]):
        self.data['media_list'] = value

    def load(self):
        self.populate(Files.config)

    def save(self):
        with open("/files/config.json", "w") as f:
            f.write(json.dumps(self.data, indent=4))

    def populate(self, path):
        # First do config file
        if os.path.exists(path):
            with open(path) as config_file:
                data = json.loads(config_file.read())
                if "telegram_token" in data:
                    self.telegram_token = data["telegram_token"]
                if "jackett_token" in data:
                    self.jackett_token = data["jackett_token"]
                if 'jackett_url' in data:
                    self.jackett_url = data["jackett_url"]
                if "transmission" in data:
                    self.transmission = LoginInfo(data["transmission"])
                    if self.transmission.user:
                        self.transmission.active = True
                if "qbittorrent" in data:
                    self.qbittorrent = LoginInfo(data["qbittorrent"])
                    if self.qbittorrent.user:
                        self.qbittorrent.active = True
                if 'progress_bar' in data:
                    self.read_progress_bar(data['progress_bar'], self.progress_bar)
                if 'media_list' in data:
                    self.media_list = data['media_list']

                self.get_values(data)

        # Then do environment variables so they override the files
        if "TELEGRAM_TOKEN" in os.environ:
            self.telegram_token = os.environ["TELEGRAM_TOKEN"]
        if "JACKETT_TOKEN" in os.environ:
            self.jackett_token = os.environ["JACKETT_TOKEN"]
        if "JACKETT_URL" in os.environ:
            self.jackett_url = os.environ["JACKETT_URL"]
        if "MEDIA_LIST" in os.environ:
            self.media_list = os.environ["MEDIA_LIST"].split(',')

        if "TRANSMISSION_USER" in os.environ and "TRANSMISSION_PASSWD" in os.environ:
            self.transmission = self.read_login_info_from_environ("TRANSMISSION")

        if "QBITTORRENT_USER" in os.environ and "QBITTORRENT_PASSWD" in os.environ:
            self.qbittorrent = self.read_login_info_from_environ("QBITTORRENT")
        self.get_values(os.environ)

    def get_values(self, data):
        self.torrent_search_limit = self.get_int(data, "torrent_search_limit", self.torrent_search_limit)
        self.torrent_display_limit = self.get_int(data, "torrent_display_limit", self.torrent_display_limit)
        self.max_item_title_length = self.get_int(data, "max_item_title_length", self.max_item_title_length)

    @staticmethod
    def read_login_info_from_environ(prefix: str):
        info = LoginInfo({})
        info.user = os.environ[f"{prefix}_USER"]
        info.password = os.environ[f"{prefix}_PASSWD"]
        info.port = int(os.environ[f"{prefix}_PORT"])
        info.active = True
        return info

    @staticmethod
    def read_progress_bar(data, progress_bar: ProgressBar):
        if 'full' in data:
            progress_bar.full = data['full']
        if 'empty' in data:
            progress_bar.empty = data['empty']
        if 'half' in data:
            progress_bar.half = data['half']
        if 'length' in data:
            progress_bar.length = data['length']
        if 'use_half' in data:
            progress_bar.use_half = data['use_half']

    @staticmethod
    def get_int(data: dict, key: str, default_value: int) -> int:
        if key in data:
            try:
                return int(data[key])
            except ValueError:
                pass
        return default_value
